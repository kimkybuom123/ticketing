import { Router } from 'express';
import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';
import { errorHandler, NotFoundError } from '@sgticket/common';

import { currentUserRouter } from './Routes/current-user';
import { signinRouter } from './Routes/signin'
import { signoutRouter } from './Routes/signOut';
import { signupRouter } from './Routes/signUp';

const app = express();
app.set('trust', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: false,
  })
);

app.use(currentUserRouter);
app.use(signinRouter);
app.use(signoutRouter);
app.use(signupRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };
