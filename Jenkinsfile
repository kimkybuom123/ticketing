podTemplate(label: 'release', 
  containers: [
    containerTemplate(name: 'kubectl', image: 'alpine/k8s:1.16.8', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'maven', image: 'baoannguyen/maven:3.5.0-jdk-8-alpine', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'docker', image: 'docker', ttyEnabled: true, command: 'cat')
  ],
  envVars: [
    secretEnvVar(key: 'DOCKERHUB_USERNAME', secretName: 'dockerhub-username-secret', secretKey: 'USERNAME'),
    secretEnvVar(key: 'DOCKERHUB_PASSWORD', secretName: 'dockerhub-password-secret', secretKey: 'PASSWORD'),
  ])
{
  node ('release') {
    def image_name = "notedemo"

    stage ('Create a testing environment for running Unit/Integration/Acceptance Tests against branch master') {
      build job: 'test', parameters: [[$class: 'StringParameterValue', name: 'GIT_BRANCH', value: 'master']]
    }

    stage('Destroy the previously created Testing environment') {
      container("kubectl") { 
        sh 'kubectl delete all -l env=testing'
      }
    }

    stage('Release the new version and push the jar artifact to Artifactory') {
      container('maven') {
        sh """
          mkdir -p /root/.ssh
          cp /home/jenkins/.ssh/id_rsa /root/.ssh/id_rsa
          chmod 400 /root/.ssh/id_rsa
          ssh-keyscan -t rsa bitbucket.org >> /root/.ssh/known_hosts
          git clone git@bitbucket.org:an_npb/note_app.git
          cd note_app
          git config --global user.email "ngphban@gmail.com"
          git config --global user.name "An Nguyen"
          mvn -B -s /etc/maven/settings.xml release:prepare "-DreleaseVersion=${RELEASE_VERSION}" "-DdevelopmentVersion=${NEW_VERSION}" release:perform -Darguments="-DskipTests"
        """
      }
    }

    stage('Build and push a new Docker image with the tag based on the release version') {
      container('docker') {
        sh """
          cd note_app
          docker login https://losttest.jfrog.io/ -u ${DOCKERHUB_USERNAME} -p ${DOCKERHUB_PASSWORD}
          docker build -t ${DOCKERHUB_USERNAME}/${image_name}:${RELEASE_VERSION} .
          docker push ${DOCKERHUB_USERNAME}/${image_name}:${RELEASE_VERSION}
        """
      }
    }
  }
}
